<?php
// check session

/**
 * dbms class
 */
class DBMS
{
  private $servername = "localhost";
  private $usernamedb = "root";
  private $passworddb = "";
  private $dbname = "webtech_01";
  private $conn = null;

  function __construct()
  {
    global $servername, $usernamedb, $passworddb, $dbname;
    try {
      $this->conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->usernamedb, $this->passworddb);
      $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
  }

  function close(){
    $this->conn = null;
  }

  function getRoleInSec($username, $nameCourse, $sec, $year, $sem){
    try {
      $stmt = $this->conn->prepare("SELECT role FROM teachcourse LEFT JOIN section ON(teachcourse.sectionID = section.id) WHERE username='$this->getUserID('$username')' and sectionID='$this->getSectionID()' ");
      $stmt->execute();
      // $row = $stmt->fetch();
      // return $row["id"];
      return $stmt->rowCount() > 0 ? $stmt->fetch()["role"]:null;
    }
    catch(PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
  }

  function createSection($courseID, $sec, $desc, $year, $sem, $time){
    try {
      // id	sec	courseID	description	years	semester	times
        $stmt = $this->conn->prepare("INSERT INTO section(sec, courseID, description,	years, semester, times)
                VALUES (:sec, :courseID, :description, :years, :semester, :times)");
        $stmt->bindParam(':sec', $sec);
        $stmt->bindParam(':courseID', $courseID);
        $stmt->bindParam(':description', $desc);
        $stmt->bindParam(':years', $year);
        $stmt->bindParam(':semester', $sem);
        $stmt->bindParam(':times', $time);

        $stmt->execute();
        return $this->conn->lastInsertId();
    }
    catch(PDOException $e)
    {
        echo "<br>" . $e->getMessage();
    }
  }

  function createCourse($id, $nameCourse, $desc){
    try {
        $stmt = $this->conn->prepare("INSERT INTO course(id, name, description)
                VALUES (:id, :name, :description)");
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':name', $nameCourse);
        $stmt->bindParam(':description', $desc);

        $stmt->execute();
    }
    catch(PDOException $e)
    {
        echo "<br>" . $e->getMessage();
    }
  }

  function teachCourse($username, $courseID, $sec, $year, $sem, $role){
    try {
        $teacherID = $this->getUserID($username);
        $sectionID = $this->getSectionID($courseID, $sec, $year, $sem);
        $stmt = $this->conn->prepare("INSERT INTO teackcourse(teacherID, sectionID, role)
                VALUES (:teacherID, :sectionID, :role)");
        $stmt->bindParam(':teacherID', $teacherID);
        $stmt->bindParam(':sectionID', $sectionID);
        $stmt->bindParam(':role', $role);

        $stmt->execute();
        return $this->conn->lastInsertId();
        //echo "New record created successfully";
    }
    catch(PDOException $e)
    {
        echo "<br>" . $e->getMessage();
    }
  }

  function getUserID($username){
    try {
      $stmt = $this->conn->prepare("SELECT id FROM user where username='$username'");
      $stmt->execute();
      // $row = $stmt->fetch();
      // return $row["id"];
      return $stmt->rowCount() > 0 ? $stmt->fetch()["id"]:null;
    }
    catch(PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
  }

  function getSectionID($courseID, $sec, $year, $sem){
    try {
      $stmt = $this->conn->prepare("SELECT id FROM section where courseID='$courseID' and sec='$sec' and years='$year' and semester='$sem'");
      $stmt->execute();
      // $row = $stmt->fetch();
      // return $row["id"];
      return $stmt->rowCount() > 0 ? $stmt->fetch()["id"]:null;
    }
    catch(PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
  }

  function takeCourse($username, $courseID, $sec, $year, $sem){
    try {
        $studentID = $this->getUserID($username);
        $sectionID = $this->getSectionID($courseID, $sec, $year, $sem);
        $stmt = $this->conn->prepare("INSERT INTO takecourse(studentID, sectionID)
                VALUES (:studentID, :sectionID)");
        $stmt->bindParam(':studentID', $studentID);
        $stmt->bindParam(':sectionID', $sectionID);

        $stmt->execute();
        return $this->conn->lastInsertId();
        //echo "New record created successfully";
    }
    catch(PDOException $e)
    {
        echo "<br>" . $e->getMessage();
    }
  }

  function changePathPicture($username, $path){
    try {
        $sql = "UPDATE user SET pathPicture='$path' WHERE username='$username'";
        // Prepare statement
        $stmt = $this->conn->prepare($sql);
        // execute the query
        $stmt->execute();
        // return to say the UPDATE succeeded
        return $stmt->rowCount() > 0 ? true : false;
    }
    catch(PDOException $e){
        echo $sql . "<br>" . $e->getMessage();
    }
  }

  function changePassword($username, $oldpasswd, $newpasswd){
    if ($this->login($username, $oldpasswd) != null){
      try {
          $sql = "UPDATE user SET password='$newpasswd' WHERE username='$username'";
          // Prepare statement
          $stmt = $this->conn->prepare($sql);
          // execute the query
          $stmt->execute();
          // return to say the UPDATE succeeded
          return $stmt->rowCount() > 0 ? true : false;
      }
      catch(PDOException $e){
          echo $sql . "<br>" . $e->getMessage();
      }
    }
  }

  function login($username, $password){
    try {
      $stmt = $this->conn->prepare("SELECT id, password FROM user where username='$username'");
      $stmt->execute();

      $row = $stmt->fetch();
      // return password_verify($password, $row["password"]) ? true:false;
      return password_verify($password, $row["password"]) ? true:false;
    }
    catch(PDOException $e) {
        echo "Error: " . $e->getMessage();
    }

  }

  function hasUsername($username){
    try {
      $stmt = $this->conn->prepare("SELECT * FROM user where username='$username'");
      $stmt->execute();
      return $stmt->rowCount() > 0 ? true:false;
    }
    catch(PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
  }

  private function addAccount($id, $username, $password, $fname, $lname, $role){
    $password_encryp = password_hash($password, PASSWORD_DEFAULT);
    try {
        $stmt = $this->conn->prepare("INSERT INTO user(id, username, password, role, firstName, lastName, pathPicture)
                VALUES (:id, :username, :password, :role, :fname, :lname, :pathPicture)");
                $stmt->bindParam(':id', $id);
                $stmt->bindParam(':username', $username);
                $stmt->bindParam(':password', $password_encryp);
                $stmt->bindParam(':role', $role);
                $stmt->bindParam(':fname', $fname);
                $stmt->bindParam(':lname', $lname);
                $stmt->bindParam(':pathPicture', $pathPicture);
        $pathPicture = '';

        $stmt->execute();
        return $this->conn->lastInsertId();
        //echo "New record created successfully";
    }
    catch(PDOException $e)
    {
        echo "<br>" . $e->getMessage();
    }
  }

  function addAdmin($id, $username, $password, $fname, $lname){
    return $this->addAccount($id, $username, $password, $fname, $lname, 'admin');
  }

  function addUser($id, $username, $password, $fname, $lname){
    return $this->addAccount($id, $username, $password, $fname, $lname, 'user');
  }
}
// echo getenv('HTTP_HOST');
echo getenv('REQUEST_URI');
// $dbms = new DBMS();
// $dbms->addAdmin("5610404231", "kasemsak","clever","kasem","Hack");
//echo $dbms->hasUsername('palm');
// echo hash('crc32', 1000, false);
// echo substr(md5('6''), 0, 8);
// if( $dbms->login('kasemsak','clever')){
//   echo $dbms->login('kasemsak','clever');
// }else{
//   echo $dbms->login('kasemsak','clever');
// }


 ?>
