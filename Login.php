<!DOCTYPE html>
<?php include "php/sessioncontrol.php" ?>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" media="screen" href="css/Deco_Login.css">
	<link href="https://fonts.googleapis.com/css?family=Cabin:600|Maven+Pro:700|Roboto+Condensed:700" rel="stylesheet">
</head>
<body>

    <div id="container">
        <h1 class="login">Login</h1>
          	<form className="loginForm" action="php/login-service.php" method="POST">
                <input class="info" type="text" placeholder="username" name="username"/>
         		<input class="info" type="password" placeholder="password" name="pw"/>
            <button type="submit" name="submit" class="loginButton">Log in</button>
          	</form>
    </div>


<!-- login button -->
</body>
</html>
